global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_line
section .text
 ; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov  rax, rdi
  .counter:
    cmp  byte [rdi], 0
    je   .end
    inc  rdi
    jmp  .counter
  .end:
    sub  rdi, rax
    mov  rax, rdi
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
	call string_length
	pop  rdi
	mov  rdx, rax
    mov  rsi, rdi
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rdx, 1
	mov rax, 1
	mov rdi, 1
	mov rsi, rsp
	syscall
	pop rdi
    ret
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	mov r8, 10
	push r12
	xor r12, r12
.loop:
	xor rdx, rdx
	div r8
	add rdx, "0"
	push rdx
	inc r12
	test rax, rax
	jne .loop
.print_loop:
	pop rdi
	call print_char
	dec r12
	test r12, r12
	jne .print_loop
	pop r12
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi
	js .print_neg
	call print_uint
	ret
.print_neg:
	push rdi
	mov rdi, "-"
	call print_char
	pop rdi
	neg rdi
	call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rdx, rdx
.loop:
	mov al, byte [rdi+rdx]
	mov ah, byte [rsi+rdx]
	cmp al, ah
	jne .not_equals
	test al, al
	jne .continue
	mov rax, 1
	ret
.continue:
	inc rdx
	jmp .loop
.not_equals:
	xor rax, rax
    ret
	

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	sub rsp, 8
    xor rax, rax
	xor rdi, rdi
	mov rsi, rsp
	mov rdx, 1
	syscall
	cmp rax, -1
	je .error
	test rax, rax
	je .error
	mov al, [rsp]
	jmp .end
.error:
	xor rax, rax 
.end:
	add rsp, 8
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r12
	push r13
	push r14
	push rdi
	mov r12, rdi
	mov r13, rsi
	xor r14, r14
.loop:
	cmp r13, r14
	jle  .error
	call read_char
	cmp al, 0
	je .success
	cmp al, `\n`
	je .handle_space
	cmp al, `\t`
	je .handle_space
	cmp al, ` `
	je .handle_space
	mov byte [r12+r14], al
	inc r14
	jmp .loop
.handle_space:
	test r14, r14
	je .loop
.success:
	pop rax
	mov byte [r12+r14], 0
	mov rdx, r14
	jmp .end
.error:
	pop rax
	xor rax, rax
.end:
	pop r14
	pop r13
	pop r12
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	mov r8, 10
	xor rdx, rdx
	xor rsi, rsi
.loop:
	mov sil, byte [rdi+rdx]
	cmp sil, "0"
	jb .return
	cmp sil, "9"
	ja .return
	push rdx
	mul r8
	pop rdx
	sub sil, "0"
	add rax, rsi
	inc rdx
	jmp .loop
.return:
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov sil, byte [rdi]
	cmp sil, "-"
	je .neg
	call parse_uint
	ret
.neg:
	inc rdi
	call parse_uint
	neg rax
	inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
.loop:
	test rdx, rdx
	je .error
	mov cl, byte [rdi]
	mov byte [rsi], cl
	cmp cl, 0
	je .return
	inc rax
	inc rdi
	inc rsi
	dec rdx
	jmp .loop
.error:
	mov rax, 0
.return:
    ret


read_line:
	push r12
	push r13
	push r14
	push rdi
	mov r12, rdi
	mov r13, rsi
	xor r14, r14
.loop:
	cmp r13, r14
	jle  .error
	call read_char
	cmp al, 0
	je .success
	cmp al, `\n`
	je .success
	mov byte [r12+r14], al
	inc r14
	jmp .loop
.success:
	pop rax
	mov byte [r12+r14], 0
	mov rdx, r14
	jmp .end
.error:
	pop rax
	xor rax, rax
.end:
	pop r14
	pop r13
	pop r12
	ret