%include "lib.inc"
global find_word
%define QWORD_SIZE 8
section .text

find_word:
	push r12
	push r13
	mov r12, rdi ; строка
	mov r13, rsi ; начало словаря
	
.loop:
	mov rdi, r12
	mov rsi, r13
	add rsi, QWORD_SIZE
	call string_equals
	test rax, rax
	jnz .found
	mov r13, [r13]
	test r13, r13
	jz .not_found
	jmp .loop
.found:
	add r13, QWORD_SIZE
	mov rax, r13
	jmp .end
.not_found:
	xor rax, rax
.end:
	pop r13
	pop r12
	ret
