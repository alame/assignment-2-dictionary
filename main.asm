
%include "lib.inc"
%include "dict.inc"

%include "words.inc"
%define MAX_SIZE 255
%define STDERR 2
%define WRITE_SYSCALL 1
global _start
section .rodata
word_too_long: db "word too long", 0
no_such_element: db "no such element", 0
section .text
_start:
	sub rsp, MAX_SIZE+1
	mov rdi, rsp
	mov rsi, MAX_SIZE
	call read_line
	test rax, rax
	je .print_word_too_long
	mov rdi, rsp
	mov rsi, one
	call find_word
	test rax, rax
	je .print_no_such_element
	mov rdi, rax
	push rdi
	call string_length
	pop rdi
	add rdi, rax
	inc rdi
	call print_string
	xor rdi, rdi
	jmp .end
.print_word_too_long:
	mov rdi, word_too_long
	jmp .print_error
.print_no_such_element:
	mov rdi, no_such_element
.print_error:
	push rdi
	call string_length
	pop  rdi
	mov  rdx, rax
    mov  rsi, rdi
    mov  rax, WRITE_SYSCALL
    mov  rdi, STDERR
    syscall
.end:
	add rsp, MAX_SIZE+1
	call exit
	      