ASM=nasm
ASMFLAGS=-f elf64

%.o:	%.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
dict.o: lib.inc
	$(ASM) $(ASMFLAGS) -o  $@  dict.asm
main.o:	lib.inc dict.inc words.inc
	$(ASM) $(ASMFLAGS) -o  $@  main.asm
program:	lib.o dict.o main.o
	ld -o  $@  $^
.PHONY: clean
clean: 
	rm *.o program

.PHONY: test
test:
	python3 test.py