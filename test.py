import subprocess


class TestResult:
    def __init__(self, input_data, success, expected_output, expected_errors, received_output, received_errors):
        self.input_data = input_data
        self.success = success
        self.expected_output = expected_output
        self.expected_errors = expected_errors
        self.received_output = received_output
        self.received_errors = received_errors

    def __str__(self):
        if self.success:
            return f"input: {self.input_data}, success: {self.success}"
        return f"input: {self.input_data}, success: {self.success}\nexpected output: {self.expected_output}, " \
            f"expected errors: {self.expected_errors}, received output {self.received_output}, " \
            f"received errors: {self.received_errors}"


class Test:
    def __init__(self, program, stdin, stdout, stderr, input_data, output, err_output):
        self.program = program
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.input_data = input_data
        self.output = output
        self.err_output = err_output

    def run(self):
        process = subprocess.Popen(self.program, stdin=self.stdin, stdout=self.stdout, stderr=self.stderr)
        stdout_data, stderr_data = process.communicate(self.input_data.encode())
        stdout_data = stdout_data.decode()
        stderr_data = stderr_data.decode()
        process.kill()
        success = stderr_data == self.err_output and stdout_data == self.output
        return TestResult(self.input_data, success, self.output, self.err_output, stdout_data, stderr_data)


tests = [Test("./program", subprocess.PIPE, subprocess.PIPE, subprocess.PIPE, "one", "1", ""),
         Test("./program", subprocess.PIPE, subprocess.PIPE, subprocess.PIPE, "three", "3", ""),
         Test("./program", subprocess.PIPE, subprocess.PIPE, subprocess.PIPE, "four", "4", ""),
         Test("./program", subprocess.PIPE, subprocess.PIPE, subprocess.PIPE, "a"*256, "", "word too long"),
         Test("./program", subprocess.PIPE, subprocess.PIPE, subprocess.PIPE, "abc", "", "no such element"),
         Test("./program", subprocess.PIPE, subprocess.PIPE, subprocess.PIPE, "fifth word", "five", ""),
         Test("./program", subprocess.PIPE, subprocess.PIPE, subprocess.PIPE, "one two", "", "no such element"),
         ]
successfully_passed_test = 0
for i in range(len(tests)):
    test = tests[i]
    test_result = test.run()
    if test_result.success:
        successfully_passed_test += 1
    print(f"test {i+1}: {test_result}")
    print("-------------------------------------------------")
print(f"successfully passed {successfully_passed_test} of {len(tests)} tests")
